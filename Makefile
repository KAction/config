all:
	@echo Supported targets:
	@echo - system: build and unpack system config into /
	@echo - config: build and unpack user config into \~/
	@echo - tgz: build config archive into out/config.tar.gz
tup:
	tup --quiet > /dev/stdout

tgz: tup
	rm -f out/config.tar.gz
	stow --no-folding --restow --dir=src/ --target=out/dest copy
	tar -hcf out/config.tar --xform='s:^out/dest/::' out/dest/
	tar --delete out/dest/ -f out/config.tar
	gzip out/config.tar

link: tgz
	stow --no-folding --adopt --restow --dir=out --target=$(HOME) dest

config: tgz
	tar -xzf out/config.tar.gz -C ~
system: tup
	sudo tar -xzf out/system.tar.gz -C /

.PHONY: all system config tup tgz

