[
  (import ./overlays.d/legacy.nix)
  (import ./overlays.d/staging.nix)
  (import ./overlays.d/rebuild.nix)
  (import ./overlays.d/config.nix)
  (import ./overlays.d/software.nix)
  (import ./overlays.d/unfortunate.nix)
  (import ./overlays.d/world.nix)
]
