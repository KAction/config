let version = "1";
in self: super: {
  world = super.lang.purify (super.buildEnv {
    name = "world-${version}"; # reference to Gentoo terminology
    paths = with super;
      builtins.concatLists [
        # Derivations that wrap programs with my configuration files, so
        # they do not need to be installed into ~/
        (builtins.attrValues configure)

        # Software that does not qualify as "config files", but still not
        # elaborate enough to warrant own repository and inclusion into
        # nixpkgs.
        (builtins.attrValues private)

        # Unmodified packages, provided by nixpkgs. Most developer tools
        # are not listed here, they belong to shell.nix of individual
        # projects instead.
        [
          # I prefer it to coreutils. Given that coreutils is part of
          # stdenv, it is not feasible to get rid of coreutils, though.
          busybox

          # syscall and libc library reference.
          man-pages

          # Small and nice file manager for cases, when I have to work
          # with unconventionally-named files.
          nnn

          # Rename is rarely needed tool, but when it is needed, it really
          # shines.
          rename

          # Nice overview of processes running on the system.
          pstree

          # Essentially, glorified bookmarks. But very useful.
          surfraw

          # How else could I poke around web api?
          rebuild.curl

          # And the most important part: Nix itself. Since I strive to
          # have only one derivation in my profile -- this one (world),
          # Nix must be included into it.
          rebuild.nix

          # Execline binaries are numerous and clutter PATH, but having
          # reference is useful.
          execline.doc

          # Beautifully simple password manager
          rebuild.pass

          # Tools for haskell development
          rebuild.cabal2nix
          rebuild.hpack
          haskellPackages.ghcid
          cabal-install
          # Hidden dependency of cabal-install. "cabal build" fails
          # if "ar" is not in PATH.
          binutils

          # Now exists tool for enforcing and automatic management of
          # copyright headers.
          reuse

          # Useful tool to follow links from text files, primarily in
          # email. Not that I receive much email lately...
          urlview

          # Sometimes I need to interact with Github. Nix itself uses it.
          # Sigh.
          configure.hub

          # GNUPG is used for more than just pass(1), it is also used for
          # email, so gpg(1) must be present in PATH.
          rebuild.gnupg
          rebuild.pinentry

          # Openssh client
          rebuild.openssh

          # Reddit is unusable in w3m
          rtv

          # Not GNU Make, to learn about Make portability
          bmake

          # I do believe in autoformatters.
          nixfmt

          # Check battery status
          acpi

          # Command line tool to use hardware token for gpg and ssh
          rebuild.trezor_agent

          # PDF reader. Unfortunately, it is hard to find prose in txt.
          fbpdf
        ]
      ];
    pathsToLink = [ "/bin" "/config" "/share/man" "/share/doc" "/etc" ];

    # These binaries require suid/capability anyway, so versions of host
    # system (/bin:/usr/bin) are to be used anyway.
    postBuild = ''
      rm $out/bin/{ping,chvt}
    '';
    meta = {
      # Set high priority, so that installing package with nix-env, and
      # then adding it to world set would not make nix-env barf about
      # collisions.
      priority = 2;
    };
  });

  # Derivations that are known to have dependencies on libshit, but I am
  # yet to figure out how to fix it.
  unfortunate = super.buildEnv {
    name = "unfortunate";
    paths = with super; (builtins.attrValues unfortunate);
    meta.priority = 2;
  };
}
