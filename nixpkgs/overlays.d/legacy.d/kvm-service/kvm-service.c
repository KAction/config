/* Wrapper around /usr/bin/kvm, that performs graceful termination
 * of virtual machine (sending ACPI poweroff message) on SIGTERM.
 */
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static int debug;
static int control_infd;

#define writefd(fd, x) write(fd, x"", sizeof(x) - 1)
#define write2(x) writefd(2, x)
#define _e(x) do { if (debug) write2(x); } while (0)

#define ensure_pipe(path) do {                                    \
	struct stat sb;                                           \
	if (stat(path, &sb) != 0) {                               \
		write2("fatal: failed to stat '" path "'\n");     \
		return 1;                                         \
	}                                                         \
	if ((sb.st_mode & S_IFMT) != S_IFIFO) {                   \
		write2("fatal: '" path "' is not fifo\n");        \
		return 1;                                         \
	}                                                         \
} while (0)

void sigterm_handler(int num)
{
	(void)(&num);
	writefd(control_infd, "system_powerdown\n");
}

int drop_privilegies()
{
	char *UID;
	uid_t uid;

	if (getuid() != 0)
		return 1;
	_e("parent: I am root. dropping privilegies.\n");

	UID = getenv("UID");
	if (!UID || !(uid = atoll(UID))) {
		write2("parent: failed to extract uid from UID variable.\n");
		return 0;
	}
	if (setgid(uid) == -1) {
		write2("parent: setgid failed\n");
		return 0;
	}
	if (setuid(uid) == -1) {
		write2("parent: setuid failed\n");
		return 0;
	}
	return 1;
}

int main(int argc, char **argv)
{
	char* new_argv[argc + 4];

	debug = !!getenv("DEBUG");

	_e("Checking that control pipes exist, since qemu do not create them.\n");
	ensure_pipe("qemu/control.in");
	_e("ok: qemu/control.in exists.\n");
	ensure_pipe("qemu/control.out");
	_e("ok: qemu/control.out exists. We do not use it,\n");
	_e("    but qemu wouldn't start without qemu/control.out.\n");
	
	new_argv[0] = "kvm";
	new_argv[1] = "-chardev";
	new_argv[2] = "pipe,id=M0,path=qemu/control";
	new_argv[3] = "-monitor";
	new_argv[4] = "chardev:M0";
	memcpy(new_argv + 5, argv + 1, argc * sizeof(void *));

	pid_t pid = fork();
	if (pid == -1) {
		write2("fatal: failed to fork\n");
		return 1;
	}

	if (pid != 0) { /* parent */
		int wstatus;

		_e("parent: opening qemu/control.in for writing.\n");
		control_infd = open("qemu/control.in", O_WRONLY | O_CLOEXEC);
		if (control_infd == -1) {
			write2("fatal: failed to open control pipe for writing\n");
			kill(pid, SIGTERM);
			return 1;
		}
		if (!drop_privilegies()) {
			write2("parent: failed to drop root privilegies\n");
			kill(pid, SIGTERM);
			return 1;
		}

		_e("parent: installing sigterm handler\n");
		signal(SIGTERM, &sigterm_handler);
		_e("parent: now waiting for child to exit\n");
		while (wait(&wstatus) != pid) /* account for EINTR */
			;
		return WIFEXITED(wstatus) ? WEXITSTATUS(wstatus) : 128 + WTERMSIG(wstatus);
	} else {
		_e("child: executing kvm with adjusted arguments:\n");
		for (char **p = new_argv; *p; ++p)
			if (debug) {
				write(2, *p, strlen(*p));
				write2("\n");
			}
		execvp(new_argv[0], new_argv);
		write2("child: exec failed, killing parent\n");
		kill(getppid(), SIGPIPE);
	}
	return 1;
}
