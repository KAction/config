{ stdenv }:
stdenv.mkDerivation rec {
  pname = "kvm-service";
  version = "1.0";
  src = ./.;
  installPhase = ''
    mkdir -p $out/bin
    cc -O2 -Wno-unused-result ${pname}.c -o $out/bin/${pname}
  '';
}
