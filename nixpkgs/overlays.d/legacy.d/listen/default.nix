{ stdenv, scdoc }:
stdenv.mkDerivation {
  pname = "listen";
  version = "1.0";
  src = ./.;
  buildInputs = [ scdoc ];
  installPhase = ''
    mkdir -p $out/bin $out/share/man/man1
    cc -O2 -Wno-unused-result listen.c -o $out/bin/listen
    scdoc < listen.txt > $out/share/man/man1/listen.1
    gzip --best $out/share/man/man1/listen.1
  '';
}
