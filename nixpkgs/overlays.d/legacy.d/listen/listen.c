#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#define write2(x) write(2, x"", sizeof(x) - 1)
#define usage() write2("usage: listen path fd prog...\n")

// Note: On unlinking
// ------------------

// If stat(2) reports, that path already exists and is socket, we unlink
// it, but do not check, whether unlink was successful. It is
// intentional.
//
// If socket disappeared between stat(2) and unlink(2) it means somebody
// else already did our job, which is fine. If unlink(2) failed, it will
// manifest itself in failed bind(2), which provides more relevant error
// message. And if socket was replaced with regular file between stat(2)
// and unlink(2), then it will be unlinked, and there is little we can
// do about it: as far as I know there is no way to prevent race
// condition here.

/* Should it be configurable? */
enum { LISTEN_BACKLOG = 10 };

int main(int argc, char **argv)
{
	const char *path;
	int fd;
	char *endptr;
	int sock;
	struct sockaddr_un sockaddr;
	struct stat sb;

	if (argc < 4) {
		usage();
		return 100;
	}
	path = argv[1];

	errno = 0;
	fd = strtol(argv[2], &endptr, 10);
	if (errno || *endptr) {
		usage();
		return 100;
	}
	
	if (strlen(path) + 1 > sizeof(sockaddr.sun_path)) {
		write2("path argument is too long\n");
		return 100;
	}

	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sock == -1) {
		perror("socket");
		return 111;
	}

	sockaddr.sun_family = AF_UNIX;
	strncpy(sockaddr.sun_path, path, sizeof(sockaddr.sun_path));

	if ((stat(path, &sb) == 0) && ((sb.st_mode & S_IFMT) == S_IFSOCK)) {
		unlink(path); /* See Note [On unlinking] */
	}

	if (bind(sock, (void*) &sockaddr, sizeof(sockaddr)) == -1) {
		perror("bind");
		close(sock);
		return 111;
	}

	if (listen(sock, LISTEN_BACKLOG) == -1) {
		perror("listen");
		unlink(path);
		close(sock);
		return 111;
	}

	if (dup2(sock, fd) == -1) {
		perror("dup2");
		unlink(path);
		close(sock);
	}

	execvp(argv[3], argv + 3);
	return 111;
}
