listen(1)

# NAME

listen - run a command with inhereted unix socket descriptor

# SYNOPSIS

listen _path_ _fd_ _command_ [_arg_]...

# DESCRIPTION

Create unix socket at _path_ and run _command_ with that socket pointed
by descriptor number _fd_. If _path_ already exists and is socket, it is
recreated, otherwise error is reported.

# ERRORS

If creation of socket fails, exit with code 111, otherwise exit with
exit code of _command_.

# AUTHOR

Dmitry Bogatov <~kaction/common@lists.sr.ht>
