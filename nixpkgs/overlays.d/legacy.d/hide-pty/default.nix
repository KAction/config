{ stdenv }:
stdenv.mkDerivation rec {
  pname = "hide-pty";
  version = "1.0";
  dontUnpack = true;
  installPhase = ''
    mkdir -p $out/bin
    cc -O2 -lutil -Wno-unused-result ${./hide-pty.c} -o $out/bin/${pname}
  '';
}
