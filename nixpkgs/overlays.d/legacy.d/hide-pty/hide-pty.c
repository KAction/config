/* This program starts specified program with pseudo-tty, discarding
 * any output it may produce.
 *
 * hide-pty.c was created as hack to start emacs server in foreground.
 * Since I no longer use emacs, hide-pty.c serves no particular reason.
 *
 * To avoid bitrot, it is compiled, but not included into config.tar.gz
 */

#include <pty.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

static sig_atomic_t return_value = 196;


static void sigchild_handler(int signum)
{
	int status;
	wait(&status);
	if (WIFEXITED(status)) {
		return_value = WEXITSTATUS(status);
	} else if (WIFSIGNALED(status)) {
		return_value = 128 + WTERMSIG(status);
	}
}

int
main(int argc, char **argv) {
	int master;
	pid_t pid;

	if (argc < 2) {
		return 3;
	}
	setenv("TERM", "linux", 1);
	signal(SIGCHLD, &sigchild_handler);

	pid = forkpty(&master, NULL, NULL, NULL);
	if (pid < 0) {
		return 2;
	}
	if (pid == 0) {
		execvp(argv[1], argv + 1);
	} else {
		char buffer[1024];
		while (read(master, buffer, sizeof(buffer)) > 0);
	}
	return 0;
}
