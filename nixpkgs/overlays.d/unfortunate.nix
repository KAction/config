self: super: {
  unfortunate = {
    firefox = super.callPackage ./config.d/firefox {
      firefox = super.firefox-esr-68;
    };
    runx = super.callPackage ./software.d/runx {
      inherit (super.xorg) xmodmap setxkbmap;
      inherit (self.private) attach-shell;
    };
  };
}
