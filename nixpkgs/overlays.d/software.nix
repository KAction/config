self: super: {
  private = {
    # This software is not for general public use.
    repoconf = super.callPackage ./software.d/repoconf {
      inherit (super.rebuild) openssh;
    };
    external-ip = super.callPackage ./software.d/external-ip {
      inherit (super.rebuild) curl;
    };
    git-recent = super.callPackage ./software.d/git-recent {
      inherit (super.rebuild) git utillinux;
    };
    attach-shell = super.callPackage ./software.d/attach-shell {
      editor = "${super.configure.vim}/bin/vim";
      dvtm = super.configure.dvtm;
    };
  };
}
