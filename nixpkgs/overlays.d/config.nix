self: super:
let
  prune = paths: drv:
    super.buildEnv {
      name = drv.name;
      paths = [ drv ];
      pathsToLink = paths;
    };
  prune' = prune [ "/bin" "/share/man" ];
  configWrapper = drv:
    { bin ? drv.pname, env }:
    let
      base = "${drv}/bin/${bin}";
      exports =
        super.lib.attrsets.mapAttrsToList (n: v: "export ${n} ${v}") env;
      wrapper = super.writeScriptBin bin ''
        #!${super.execline}/bin/execlineb -S0
        ${builtins.concatStringsSep "\n" exports}
        ${drv}/bin/${bin} $@
      '';
    in super.buildEnv {
      name = drv.name;
      paths = [ wrapper drv ]; # wrapper must be first to override
      ignoreCollisions = true;
    };
  xdgConfigWrapper = drv: path:
    configWrapper drv { env = { XDG_CONFIG_HOME = path; }; };
in {
  configure = {
    git = self.callPackage ./config.d/git { inherit (super.rebuild) git; };
    tig = xdgConfigWrapper super.rebuild.tig ./config.d/git;
    hub = self.callPackage ./config.d/hub {
      inherit (super.rebuild) hub;
      inherit (super.rebuild) pass;
    };
    mpop = self.callPackage ./config.d/mpop { };
    vim = self.callPackage ./config.d/vim { };
    msmtp = xdgConfigWrapper super.rebuild.msmtp ./config.d/msmtp;
    ctags = super.callPackage ./config.d/ctags { };
    mmh = super.callPackage ./config.d/mmh {
      inherit (self.configure) msmtp;
    };
    htop = super.callPackage ./config.d/htop { };
    dvtm = super.callPackage ./config.d/dvtm { };
    cfg-mksh-general = super.callPackage ./config.d/mksh-general { };
    cfg-mksh-surfraw = super.callPackage ./config.d/mksh-surfraw { };
    cfg-mksh-cutters = super.callPackage ./config.d/mksh-cutters { };
    cfg-mksh-mitmproxy = super.callPackage ./config.d/mksh-mitmproxy { };
    w3m = let
      w3m-home = super.stdenv.mkDerivation {
        name = "w3m-home";
        src = ./config.d/w3m.conf;
        # There is no use to link "history" to /dev/null, since
        # w3m tries to rename(2) into ~/.w3m/history, not write.
        installPhase = ''
          mkdir -p $out/.w3m
          cp $src $out/.w3m/config
          ln -sf /dev/null $out/.w3m/cookie
        '';
        phases = [ "installPhase" ];
      };
    in configWrapper super.rebuild.w3m { env.HOME = w3m-home; };
  };
}
