{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  pname = "snooze";
  version = "0.3+";
  src = fetchFromGitHub {
    owner = "leahneukirchen";
    repo = "snooze";
    rev = "73f3ccf";
    sha256 = "1a3h5njj93iw9i8blkvl900mlans01hpg5a58r05fpmhnq0ckl1a";
  };
  makeFlags = [ "DESTDIR=$(out)" "PREFIX=/" ];
}
