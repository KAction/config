{ stdenv, prefetched, lib, mupdf, openssl, supportDjvu ? true, djvulibre
, libjpeg }:
assert supportDjvu -> djvulibre != null && libjpeg != null;
stdenv.mkDerivation {
  pname = "fbpdf";
  version = "2018.01.05";
  src = prefetched ./src.json;
  buildInputs = [ mupdf openssl ]
    ++ lib.optional supportDjvu [ djvulibre.dev libjpeg.dev ];

  patches = [ ./patches/format.patch ./patches/current-page.patch ];
  makeFlags = [ "LDFLAGS=-lcrypto" "fbpdf" ]
    ++ lib.optional supportDjvu [ "fbdjvu" ];
  installPhase = ''
    mkdir -p $out/bin $out/share/fbpdf
    cp fbpdf $out/bin
    cp README $out/share/fbpdf
  '' + lib.optionalString supportDjvu ''
    cp fbdjvu $out/bin
  '';
  meta = with lib; {
    homepage = "https://github.com/aligrudi/fbpdf";
    description = "Framebuffer pdf and djvu viewer";
    license = licenses.mit;
    platforms = platforms.linux;
  };
}
