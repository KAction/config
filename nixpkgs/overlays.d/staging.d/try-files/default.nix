# [Path] -> Maybe Path
#
# Return first path in list that exist, or null if none does.
files:
  let existing = builtins.filter builtins.pathExists files;
  in if existing == [] then null else builtins.head existing
