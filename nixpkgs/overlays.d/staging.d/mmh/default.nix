{ stdenv, fetchgit, ncurses, flex, autoconf, automake }:
stdenv.mkDerivation rec {
  pname = "mmh";
  version = "0.4+";
  buildInputs = [ ncurses flex autoconf automake ];
  patches = [ ./patches/adjust-bash-completion-script.patch ];
  src = fetchgit {
    url = "https://git.sr.ht/~kaction/mmh";
    rev = "1314055f94b88d61ca92867ae6ec5adb41479df8";
    sha256 = "1y71z52wsvdrv95f99wl02qjs2h4zq53d0x92a2na7wxclwgvnzi";
  };
  preConfigure = "./autogen.sh";
  meta = with stdenv.lib; {
    description = "Set of electronic mail handling programs";
    homepage = "http://marmaro.de/prog/mmh";
    license = licenses.bsd3;
    platforms = platforms.unix;
  };
}
