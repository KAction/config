self: super: {
  mmh = self.callPackage ./staging.d/mmh { };
  snooze = self.callPackage ./staging.d/snooze { };
  prefetched = self.prefetchedGit;
  prefetchedGit = path:
    let info = builtins.fromJSON (builtins.readFile path);
    in super.fetchgit { inherit (info) rev url sha256; };
  lang = {
    tryFiles = import ./staging.d/try-files;

    # This is the best way I found to ensure that my profile has no
    # runtime dependencies of some packages (die, libsystemd, die!).
    #
    # Due the way Nix works, it will actually build derivation
    # (actually, download substitutes) for systemd, pam and other junk
    # just to make sure they are not retained in closure. So, this junk
    # will always be in Nix store.
    purify =
      let forbidded = with super; [ systemd systemd.lib pam kerberos dbus ];
      in drv: drv.overrideAttrs (_: { disallowedRequisites = forbidded; });
  };
  fbpdf = self.callPackage ./staging.d/fbpdf { };
  savepage =
    let source = self.prefetched ./_prefetch/savepage.json;
    in  self.callPackage (source + /savepage.nix) { };
}
