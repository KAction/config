self: super: {
  rebuild = {
    hub = super.gitAndTools.hub.overrideAttrs (_: {
      src = super.fetchgit {
        url = "https://git.sr.ht/~kaction/hub";
        rev = "871b1308092bda000a24626d9b5f51921974fcfb";
        sha256 = "01qjjypxd25yr843dprfr1qq0gryknk4p4dhz20033v4ycsd2yg0";
      };
    });

    nix = super.nix.override {
      inherit (self.rebuild) curl;
      withAWS = false;
    };

    curl = super.curl.override {
      scpSupport = false;
      gssSupport = false;
      ldapSupport = false;
      http2Support = false;
      idnSupport = false;
    };

    libusb1 = with super;
      let
        libusb1' = libusb1.override { enableSystemd = false; };
        libusb1'' = libusb1'.overrideAttrs (_: {
          configureFlags = [ "--enable-udev=no" ];
          preFixup = "";
          disallowedRequisites = with super; [ systemd pam dbus ];
        });
      in libusb1'';

    python3Packages = {
      libusb1 = super.python3Packages.libusb1.override {
        inherit (self.rebuild) libusb1;
      };

      hidapi = with super;
        let
          hidapi' = python3Packages.hidapi.override {
            inherit (self.rebuild) libusb1;
            udev = eudev;
          };
        in hidapi';

      keepkey = with super;
        let
          keepkey' = python3Packages.keepkey.override {
            inherit (self.rebuild.python3Packages) trezor hidapi;
          };
        in keepkey';

      trezor = with super;
        let
          trezor' = python3Packages.trezor.override {
            inherit (self.rebuild.python3Packages) libusb1 hidapi;
          };
        in trezor';
    };

    trezor_agent = super.trezor_agent.override {
      inherit (self.rebuild) pinentry;
      inherit (self.rebuild.python3Packages) trezor keepkey;
    };

    openssh = super.openssh.override {
      withKerberos = false;
      pam = null;
    };
    shadow = super.shadow.override { pam = null; };
    utillinux = (super.utillinux.override {
      inherit (self.rebuild) shadow;
      pam = null;
      systemd = null;
      ncurses = null;
    }).overrideAttrs (_: { buildInputs = [ super.zlib ]; });
    git = (super.git.override {
      guiSupport = false;
      pythonSupport = false;
      sendEmailSupport = false;
      svnSupport = false;
      withLibsecret = false;
      withpcre2 = false;
      inherit (self.rebuild) openssh curl;
    }).overrideAttrs (_: { installCheck = ""; });
    tig = super.tig.override { inherit (self.rebuild) git; };
    gnupg = let
      gnupg_ = super.gnupg.override {
        openldap = null;
        libusb = null;
      };
    in gnupg_.overrideAttrs (old: rec { postPatch = ""; });
    procps = super.procps.override { withSystemd = false; };
    w3m = super.w3m.override {
      graphicsSupport = false;
      imlib2 = null;
      x11Support = false;
      mouseSupport = false;
    };
    pinentry = super.pinentry.override {
      libcap = null;
      libsecret = null;
      enabledFlavors = [ "tty" ];
    };
    pass = super.pass.override {
      x11Support = false;
      waylandSupport = false;
      qrencode = super.coreutils;
      inherit (self.rebuild) git gnupg procps;
    };
    msmtp = super.msmtp.override {
      withKeyring = false;
      systemd = "/banish/systemd";
    };
    gdb = super.gdb.override {
      pythonSupport = false;
      guile = null;
    };

    nix-prefetch-git = super.nix-prefetch-git.override {
      inherit (self.rebuild) git;
    };

    # We need only binary, not library.
    hpack = super.runCommand "hpack" {} ''
      mkdir -p $out/bin
      cp ${super.haskellPackages.hpack}/bin/* $out/bin
    '';

    cabal2nix = let
      cabal2nix' =
        let old = super.nix-prefetch-scripts;
            new = self.rebuild.nix-prefetch-git;
        in super.cabal2nix.overrideAttrs (_: {
          postFixup = ''
            sed -i 's,${old},${new},g' $out/bin/*
          '';
        });
      cabal2nix'' = super.replaceDependency {
        drv = cabal2nix';
        oldDependency = super.nix;
        newDependency = self.rebuild.nix;
      };
      cabal2nix''' = super.replaceDependency {
        drv = cabal2nix';
        oldDependency = super.git;
        newDependency = self.rebuild.git;
      };
    in cabal2nix'';
  };
}
