{ substituteAll, less, mksh }:
substituteAll {
  src = ./general.sh;
  dir = "/config/sh";
  inherit less mksh;
  inputrc = ./inputrc;
}
