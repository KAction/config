export EMAIL='KAction@debian.org'
export DEBEMAIL='KAction@debian.org'
export DEBFULLNAME='Dmitry Bogatov'
export EDITOR='vim'
export BROWSER='w3m'
export NNN_USE_EDITOR=1
export SURFRAW_browser='w3m'
export LANG='C.UTF-8'
export SSH_AUTH_SOCK="${HOME}/.gnupg/S.gpg-agent.ssh"
export GPG_TTY="$(tty)"
export HOSTNAME="$(hostname -s)"
export LESSHISTFILE="/dev/shm/${USER}/.lesshst"
export MANPATH="${HOME}/.nix-profile/share/man:/usr/share/man"
export TERMINFO="@dvtm@/share/terminfo:/usr/share/terminfo"
export INPUTRC=@inputrc@
export XDG_DATA_HOME="/dev/shm/${USER}"
export GNUPGHOME=${HOME}/upload/gnupghome
# Most programs that use less(1) as default pager are not happy with
# busybox implementation of less. This way I force them to not use
# pager at all, and if I wish, I can add '|less -RS' myself.
#
# It works better.
export PAGER=cat
export LESS=-RS
unset ENV

KEY_GPG_MASTER='7214B3C8D5FF8F26E12593D52E20FEEE71FC7D81'
KEY_GPG_SIGN='8671D5CC36ED747EE4B4A8F84812D8DEA82611E6'
KEY_GPG_AUTH='3DCBBFB61F35293F3FF9ED5099CFF0FCE633C826'
KEY_GPG_ENCR='A856CA76841C14506C00A72DA887F3D1A113FE37'

PS1=$'[$HOSTNAME]($?) $PWD \n${IN_NIX_SHELL:+(}${IN_NIX_SHELL:-}${IN_NIX_SHELL:+)}$ '

set -o vi
umask 077

alias ls='ls -FAg'
alias man='PAGER=less man'
alias ddg='sr-duckduckgo '

__paginate() {
	"$@" 2>&1 | @less@/bin/less
}
alias %=__paginate

# tig(1) refuses to create $XDG_DATA_HOME/tig itself if it does not
# already exists, and fallbacks on ~/.tig_history
mkdir -p "/dev/shm/${USER}/tig"
