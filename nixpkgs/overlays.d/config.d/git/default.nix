{ git, perl, execline, runCommand }:
let
  env = {
    gitconfig = ./gitconfig.in;
    inherit git execline perl;
  };
in runCommand "git" env "source ${./builder.bash}"
