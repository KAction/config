#!/bin/bash -- source only
mkdir $out/config/git -p
cp -t $out -rs $git/*

chmod u+w $out/bin && unlink $out/bin/git

cat << EOF > $out/bin/git
#!${execline}/bin/execlineb -S0
export XDG_CONFIG_HOME $out/config
${git}/bin/git \$@
EOF
chmod +x $out/bin/git

substituteAll $gitconfig $out/config/git/config
