" Minor amendments to gitcommit.vim syntax file to encourage formatting
" commit messages more verbosely and informative. Following is added:
"
" * Highlight `m4-style' quotation. Personally, I often mix this style
"   with `markdown` one. With highlighting it is easier to be
"   consistent.
"
" * Highlight GNU-style changelog entries, like following:
"
"     * src/foo.c: reworked i/o subsistem.
"     * src/bar.c(output_object): rewrite function to avoid use of
"       stdio.
"
" * Make mode suitable for editing src(1) commit messages too.

syn region gitcommitEntry start="^ \* " end=":" transparent
  \ contains=gitcommitFilePath,gitcommitAffected
syn match gitcommitBullet "^ \*" contained
syn match gitcommitFilePath "\v^ \* [a-zA-Z0-9./_-]+" contained contains=gitcommitBullet
syn match gitcommitAffected "\v\(\zs[a-zA-Z0-9_-]+\ze\)" contained
syn match gitcommitQuote "\v`\zs[a-zA-Z/.-]+\ze'"
syn region gitcommitComment start="^\.\.\." end="^Changes to be committed:"
syn region gitcommitDiff start=/^--- / end=/______/ contains=@gitcommitDiff

hi! def link gitcommitBullet gitcommitComment
hi! def link gitcommitFilePath Type
hi! def link gitcommitBullet Title
hi! def link gitcommitAffected Identifier
hi! def link gitcommitQuote Constant
