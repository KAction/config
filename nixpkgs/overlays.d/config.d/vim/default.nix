{ buildEnv, makeWrapper, vim, vimPlugins }:
buildEnv {
  inherit (vim) name;
  ignoreCollisions = true;
  paths = let plugins = with vimPlugins; [ vim-nix dhall-vim ];
  in [ ./vim vim ] ++ plugins;
  meta = { priority = 3; };
  buildInputs = [ makeWrapper ];
  postBuild = ''
    for path in $out/share/vim-plugins/* ; do
        echo "set rtp+=$path" >> $out/share/vim/vim82/vimrc.prefix
    done
    cat $out/share/vim/vim82/vimrc{.prefix,} > $out/share/vim/vim82/vimrc.new
    mv $out/share/vim/vim82/vimrc{.new,}
    wrapProgram $out/bin/vim --set VIM $out/share/vim/vim82
    rm $out/bin/.keepit
  '';
}
