{ substituteAll, execline, runCommand, mpop, procmail, bogofilter, configure }:
let
  procmail_config = substituteAll {
    src = ./procmailrc.in;
    inherit (configure) mmh;
    inherit (configure.mmh) confdir;
    inherit bogofilter procmail;
  };
  mpop-config = substituteAll {
    name = "config";
    dir = "mpop";
    src = ./mpoprc.in;
    inherit procmail procmail_config;
  };
in runCommand "mpop" { } ''
  mkdir -p $out/bin
  ln -sf ${mpop}/share $out/share
  cat << EOF > $out/bin/mpop
  #!${execline}/bin/execlineb -S0
  export XDG_CONFIG_HOME ${mpop-config}
  ${mpop}/bin/mpop \$@
  EOF
  chmod +x $out/bin/mpop
''
