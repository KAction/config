#!/bin/bash -- source-only

# SPDX-FileCopyrightText: 2020 Dmitry Bogatov <KAction@disroot.org>
#
# SPDX-License-Identifier: AGPLv3

# Create alias named "sr-foo" for each surfraw module (elvi).  Unlike
# "sr foo" they are nicely auto-completed even without programmable
# completion interface.

mkdir -p $out/config/sh
exec >> $out/config/sh/surfraw.sh

cd "${surfraw}/lib/surfraw"
for elvi in * ; do
	echo "alias sr-${elvi}='${surfraw}/bin/sr ${elvi}'"
done
