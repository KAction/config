{ runCommand, surfraw }:
let env = { inherit surfraw; };
in runCommand "alias.surfraw" env ". ${./builder.bash}"
