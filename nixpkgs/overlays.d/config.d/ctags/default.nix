{ ctags, execline, substituteAll, buildEnv }:
let
  wrapper = substituteAll {
    src = ./ctags.in;
    inherit execline ctags;
    isExecutable = true;
  };
in buildEnv {
   inherit (ctags) name;
   paths = [ ctags ];
   pathsToLink = [ "/" "/bin" ];
   postBuild = "cp -fv ${wrapper} $out/bin/ctags";
}
