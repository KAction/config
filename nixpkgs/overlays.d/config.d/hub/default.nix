{ hub, runCommand, execline, pass }:
let env = { inherit hub execline pass; };
in runCommand "hub" env ''
  . ${./builder.bash}
''
