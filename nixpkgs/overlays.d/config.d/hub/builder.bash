#!/bin/bash -- shebang is not used, script is sourced
set -eu
mkdir $out
cp -t $out -rs $hub/*
chmod u+w $out/bin
unlink $out/bin/hub
cat << EOF > $out/bin/hub
#!${execline}/bin/execlineb -S0
export GITHUB_USER kaction
export GITHUB_PROTOCOL https
importas -in USER USER
define unencrypted /dev/shm/\${USER}/github.txt
backtick -in GITHUB_TOKEN {
	ifelse { test -r \$unencrypted } {
		cat \$unencrypted
	} pipeline {
		pass access/token/github.com/kaction
	} tee \$unencrypted
}
${hub}/bin/hub \$@
EOF
chmod +x $out/bin/hub
