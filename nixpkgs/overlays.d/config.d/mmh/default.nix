{ busybox, mmh, execline, buildEnv, bogofilter, msmtp, substituteAll, runCommand
, lang }:
let
  env.pager = "${busybox}/bin/less";
  env.sendmail = "${msmtp}/bin/msmtp";
  env.aliases = lang.tryFiles [ ~/upload/aliases.txt ./empty.txt ];
  mmh_config = runCommand "mmh-config" env ''
    mkdir -p $out
    cp ${./config}/* $out
    substituteAll $out/profile $out/profile.out
    mv $out/profile.out $out/profile
  '';
  shell_config = substituteAll {
    src = ./mmh.sh;
    dir = "/config/sh";
    inherit mmh_config;
  };
in buildEnv {
  inherit (mmh) name;
  paths = [ mmh shell_config ];
  pathsToLink = [ "/" "/bin" ];
  postBuild = ''
    export extra=${./extra}
    export mmh=${mmh}
    export bogofilter=${bogofilter}
    export execline=${execline}

    . ${./builder.bash}
  '';
  passthru = { confdir = mmh_config; };
}
