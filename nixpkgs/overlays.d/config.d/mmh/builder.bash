#!/bin/bash -- source-only
cd ${extra}
for src in ${extra}/*.in ; do
	bin=$(basename "${src}" .in)
	rm -f "${out}/bin/${bin}"
	substituteAll "${src}" "$out/bin/${bin}"
	chmod +x "${out}/bin/${bin}"
done
