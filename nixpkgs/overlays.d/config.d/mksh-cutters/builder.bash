#!/bin/bash -- source-only

# SPDX-FileCopyrightText: 2020 Dmitry Bogatov <KAction@disroot.org>
#
# SPDX-License-Identifier: AGPLv3

# Create 'cutter-<foo>' alias for every template in ./cutters.

set -eu
readonly prog="${cookiecutter}/bin/cookiecutter"

mkdir -p $out/config/sh
exec >> $out/config/sh/cutters.sh

cd "${cutters}"
for c in * ; do
	echo "alias cutter-${c}='${prog} -f ${cutters}/${c}'"
done
