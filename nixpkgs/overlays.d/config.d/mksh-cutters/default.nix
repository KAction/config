{ cookiecutter, runCommand }:
let env = { inherit cookiecutter; cutters = ./cutters; };
in runCommand "alias.cutters" env ". ${./builder.bash}"
