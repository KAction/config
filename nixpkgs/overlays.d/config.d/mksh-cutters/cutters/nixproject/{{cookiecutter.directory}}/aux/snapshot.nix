let # NixOS release 19.09
  rev = "d5291756487d70bc336e33512a9baf9fa1788faf";
  sha256 = "0mhqhq21y5vrr1f30qd2bvydv4bbbslvyzclhw0kdxmkgg3z4c92";
in let url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
in import (builtins.fetchTarball { inherit url sha256; }) { }
