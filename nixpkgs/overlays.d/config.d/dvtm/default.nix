{ dvtm-unstable }:
let
  addPatches = drv: newPatches:
    drv.overrideAttrs (old: { patches = old.patches ++ newPatches; });
in addPatches dvtm-unstable [
  ./patches/restore-signal-handlers.patch
  ./patches/highlight-selected-tag.patch
  ./patches/increase-number-of-tags-to-9.patch
  ./patches/fix-spelling-errors.patch
]
