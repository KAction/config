{ substituteAll, execline, firefox, busybox, buildEnv }:
let
  wrapper = substituteAll {
    src = ./firefox.exec;
    inherit firefox busybox execline;
    userjs = ./user.js;
    isExecutable = true;
  };
in buildEnv {
  inherit (firefox) name;
  paths = [ firefox ];
  pathsToLink = [ "/" "/bin" ];
  postBuild = ''
    rm $out/bin/firefox
    cp ${wrapper} $out/bin/firefox
  '';
}
