{ execline, htop, buildEnv, substituteAll }:
let
  wrapper = substituteAll {
    src = ./htop.exec;
    name = "htop";
    dir = "/bin";
    inherit htop execline;
    config = ./htoprc;
    isExecutable = true;
  };
in buildEnv {
  inherit (htop) name;
  paths = [ wrapper htop ]; # first one wins.
  ignoreCollisions = true;
}

