#!/bin/bash -- source-only
set -eu
mkdir -p $out/config/sh

exec >>$out/config/sh/mitmproxy.sh

cd "${mitmproxy}/bin"
for bin in mitm* ; do
	echo "alias ${bin}='${mitmproxy}/bin/${bin}'"
done
