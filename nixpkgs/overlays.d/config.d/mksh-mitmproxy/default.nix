{ runCommand, mitmproxy }:
let env = { inherit mitmproxy; };
in runCommand "alias.mitmproxy" env ". ${./builder.bash}"
