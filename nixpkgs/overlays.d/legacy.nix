self: super: {
  legacy = {
    listen = super.callPackage ./legacy.d/listen { };
    hide-pty = super.callPackage ./legacy.d/hide-pty { };
    kvm-service = super.callPackage ./legacy.d/kvm-service { };
  };
}
