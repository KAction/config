{ substituteAll, git, mksh, utillinux }:
substituteAll rec {
  src = ./git-recent.in;
  name = "git-recent";
  dir = "/bin";
  inherit git mksh utillinux;
  postInstall = "chmod +x $out/bin/${name}";
}
