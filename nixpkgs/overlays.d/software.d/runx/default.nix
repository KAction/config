{ dwm, st, execline, busybox, xinit, xmodmap, setxkbmap, attach-shell
, writeScriptBin, dmenu }:
let
  dwm' = dwm.overrideAttrs (_: {
    postPatch = "substituteAll ${./config.h.in} config.def.h";
    shell = "${attach-shell}/bin/attach-shell";
    inherit st dmenu;
  });
in writeScriptBin "runx" ''
  #!${execline}/bin/execlineb -P
  ${xinit}/bin/xinit
  ${busybox}/bin/env TERMINFO=${st}/share/terminfo:
  foreground { ${setxkbmap}/bin/setxkbmap
  	-layout "us,ru"
  	-option
  	-option 'lv3:switch,misc:typo,caps:escape,grp:shifts_toggle'
  }
  foreground { ${xmodmap}/bin/xmodmap ${./Xmodmap} }
  ${dwm'}/bin/dwm
''
