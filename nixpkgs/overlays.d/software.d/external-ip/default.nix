{ execline, curl, gnugrep, writeScriptBin }:
writeScriptBin "external-ip" ''
  #!${execline}/bin/execlineb
  pipeline {
    ${curl}/bin/curl -s http://checkip.dyndns.org:8245
    } ${gnugrep}/bin/grep -Eo "([0-9]{1,3}\\.?){4}"
''
