export SHELL=@mksh@/bin/mksh
for config in ~/.nix-profile/config/sh/*.sh ; do
	. "${config}"
done

alias into-env='ENV=@out@ nix-shell --command $SHELL'
if [ -f ~/.posixrc.override ] ; then
	. ~/.posixrc.override
fi
