{ dvtm, mksh, execline, busybox, abduco, glibcLocales, substituteAll, editor }:
let
  config = substituteAll {
    src = ./mksh.sh;
    inherit mksh;
  };
  shell = substituteAll {
    src = ./shell.exec;
    isExecutable = true;
    inherit busybox config execline mksh;
  };
  attach-shell = substituteAll {
    name = "attach-shell";
    dir = "/bin";
    src = ./attach-shell.exec;
    isExecutable = true;
    inherit abduco busybox dvtm editor execline glibcLocales shell;
  };
in attach-shell
