{ runCommand, openssh, busybox }:
let
  env = {
    inherit busybox openssh;
    newrepo_remote = ./newrepo-remote;
  };
in runCommand "repoconf" env ''
  mkdir -p $out/bin
  substituteAll ${./K-newrepo.in} $out/bin/K-newrepo
  chmod +x $out/bin/*
''
