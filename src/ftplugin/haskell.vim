
setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal textwidth=79
setlocal equalprg=brittany

nnoremap <buffer> <leader>= m'ggVG=''

command! -bang -nargs=1 -complete=tag HSymbol call hsymbol#main(<f-args>, "<bang>")
command! -bang HSymbolWord call hsymbol#main(expand("<cword>"), "<bang>")
nnoremap <buffer> <leader>i :HSymbolWord<CR>
nnoremap <buffer> <leader>I :HSymbolWord!<CR>

au BufWrite *.hs :silent !vtags --silent 2>/dev/null &
