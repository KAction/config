setlocal tabstop=4
setlocal shiftwidth=4

if expand("%:t") ==# '.posixrc'
  let &path = printf("%s/prefix/config/posix", expand("%:h"))
  setlocal suffixesadd+=.sh
endif
