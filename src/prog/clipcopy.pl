#!/usr/bin/env perl
use strict;
use warnings;
my %words = (
    'name'   => 'Dmitry', 
    'family' => 'Bogatov',
    'phone'  => '954 487 9952'
    );
my @keys = keys(%words);
my $buttonsarg = join(',', @keys);
my @args = ('-nearmouse', '-buttons', $buttonsarg, 'Copy-Paste');

my $err = system 'xmessage', @args;
my $ix = ($err >> 8) - 101;
my $value = $words{$keys[$ix]};
exec 'xdotool', ('type', $value);
