from subprocess import check_output, check_call
import sys
import subprocess


def _invoke_worker(args, kwargs, lines=True, optional=False, capture=False):
    fn = [check_call, check_output][capture]
    try:
        result = fn(args, universal_newlines=True, **kwargs)
        if capture and lines:
            result = result.strip('\n').split('\n')
            if result[-1] == '':
                result = result[:-1]
        return result
    except subprocess.CalledProcessError as e:
        if not optional or capture:
            raise e


def invoke(*args, **kwargs):
    """
    Wrapper around subprocess.check_call with nicer interface.

    Instead of passing arguments as list, they are passed
    as variaiable number of arguments.
    """
    return lambda **opts: _invoke_worker(args, kwargs, **opts)

def invoke2(*args, **kwargs):
    """
    Same as {invoke}, but separates arguments on whitespace.
    To preserve arguments with whitespace, pass them in list.
    """
    new_args = []
    for arg in args:
        if type(arg) is list:
            new_args += arg
        else:
            new_args += arg.split()
    return lambda **opts: _invoke_worker(new_args, kwargs, **opts)


def assert_git_worktree_clean():
    """
    Terminate program, if working tree is dirty.
    """
    output = invoke('git', 'status', '--porcelain')(capture=True)
    if output:
        sys.stderr.write('fatal: working directory is dirty.\n')
        sys.stderr.writelines('\t' + line + '\n' for line in output)
        sys.exit(1)
