{
  packageOverrides = pkgs: rec {
    dwm = pkgs.dwm.overrideAttrs (old: rec {
      buildPhase = let config = ./override/dwm+config.h;
                   in "cp ${config} config.h; make";
    });
    rc-with-completion = pkgs.rc.overrideAttrs (old: rec {
      patches = [ ./patches/rc+external-completer.patch ];
    });
    world = pkgs.buildEnv {
      name = "world";
      paths =
        let vim-extras = with pkgs.vimPlugins;
          [ rust-vim vim-go vim-nix tlib snipmate vim-addon-mw-utils ];
        in vim-extras ++ (with pkgs; [ dwm gitAndTools.hub watchman ]);
    };
    asmutils = pkgs.callPackage ./extra/asmutils.nix {};
  };
}

