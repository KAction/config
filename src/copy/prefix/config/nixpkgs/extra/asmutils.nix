{ stdenv, fetchurl, nasm, rename, which }:

stdenv.mkDerivation rec {
  name = "asmutils-0.18";
  src = fetchurl {
    url = "http://asm.sourceforge.net/asmutils/${name}.tar.gz";
    sha256 = "12nk2vxhnzbkp1nsm6cah0yyvnixa02qmzs3l9jax1cw3i95hplh";
  };
  preBuild = ''
    sed -i '/^MAKE/ d' MCONFIG
    sed -i '/^INSTALLDIR/ d' MCONFIG
  '';
  buildPhase = ''
    export PATH="$PATH:${nasm}/bin:${which}/bin"
    make -C src
  '';
  installPhase = ''
    export PATH="$PATH:${nasm}/bin:${which}/bin:${rename}/bin"
    mkdir -p $out/share/doc/asmutils/
    cp doc/* $out/share/doc/asmutils/

    make -C src install INSTALLDIR=$out/bin
    cd $out/bin && rename 's/^/asmutils-/' * \
                && find -xtype l -delete
  '';
}
