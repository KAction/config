(eval-when-compile (require 'cl-lib))

(ignore-errors
  (set-locale-environment "C.UTF-8"))

(defmacro with-hook (hook &rest body)
  "Add to value of HOOK function, that evalutes BODY."
  (declare (indent defun))
  `(add-hook ',hook (lambda () ,@body)))

(defmacro run (&rest body)
  "Make interactive command from BODY of statements."
  `(lambda (&rest _ignore) (interactive) ,@body))

(cl-defun demand! (feature &optional (package feature))
  "Require FEATURE, installing PACKAGE if necessery."
  (unless (package-installed-p package)
    (package-install package))
  (require feature))

(defmacro ~disable (mode)
  "Expand to interactive function that disable MODE."
  `(lambda ()
     (interactive)
     (,mode -1)))

(defun ~quote-current-symbol()
  "Quote current symbol in m4 style (<backtick> and <quote>)."
  (interactive)
  (pcase-let*
      ((`(,start . ,end) (bounds-of-thing-at-point 'symbol))
       (point (point)))
    (atomic-change-group
      (save-excursion
        (goto-char end)
        (insert "'")
        (goto-char start)
        (insert "`"))
      (when (equalp point end)
        (forward-char 1)
        (insert " ")))))

(require 'package)
(setq package-archives
      '(("m:gnu"          . "~/prefix/mirror/emacs/elpa")
        ("m:melpa-stable" . "~/prefix/mirror/emacs/melpa-stable")))
(package-initialize)

(demand! 'cider)
(demand! 'clojure-mode)
(demand! 'company)
(demand! 'company-irony)
(demand! 'company-c-headers)
(demand! 'css-mode)
(demand! 'debbugs)
(demand! 'evil)
(demand! 'evil-magit)
(demand! 'evil-paredit)
(demand! 'flycheck)
(demand! 'magit)
(demand! 'names)
(demand! 'nameless)
(demand! 'paredit)
(demand! 'rc-mode)
(demand! 'intero)
(demand! 'org)
(demand! 'cargo)
(demand! 'fullframe)
(demand! 'diredfl)

(require 'cc-vars)
(require 'evil-core)

(when window-system
  (require 'kosmos-theme)
  (load-theme 'tango-dark :force)
  (load-theme 'alect-dark :force)
  (load-theme 'kosmos :force)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  (tool-bar-mode -1))

(unless window-system
  (set-face-attribute 'company-tooltip nil :foreground "black")
  (set-face-attribute 'company-tooltip nil :weight 'bold))

(evil-mode)
(global-company-mode)
(global-whitespace-mode)

(evil-define-key 'insert global-map (kbd "C-n") #'company-complete)
(evil-define-key 'normal intero-mode-map (kbd "C-]") #'intero-goto-definition)
(evil-define-key 'insert text-mode-map (kbd "C-/") #'~quote-current-symbol)
(evil-define-key 'normal paredit-mode-map
  (kbd "; <") #'paredit-forward-barf-sexp
  (kbd "; >") #'paredit-forward-slurp-sexp
  (kbd "; s") #'paredit-splice-sexp)
(evil-define-key 'normal emacs-lisp-mode-map
  (kbd "; e") #'eval-last-sexp
  (kbd "; d") #'eval-defun)
(dolist (keymap (list company-search-map company-active-map))
  (define-key keymap (kbd "C-n") #'company-select-next)
  (define-key keymap (kbd "C-p") #'company-select-previous))
(define-key evil-normal-state-map "Q" #'delete-other-windows)
(define-key evil-normal-state-map "|" #'switch-to-buffer)
(dolist (keymap (list package-menu-mode-map help-mode-map))
  (define-key keymap "j" #'next-line)
  (define-key keymap "k" #'previous-line))
(evil-define-key 'normal org-mode-map
  (kbd ">") #'org-shiftmetaright
  (kbd "<") #'org-shiftmetaleft)

(define-key magit-status-mode-map (kbd "q") #'quit-window)
(define-key package-menu-mode-map "/" #'isearch-forward)
(define-key package-menu-mode-map "?" #'isearch-backward)
(define-key package-menu-mode-map "n" #'isearch-repeat-forward)
(define-key package-menu-mode-map "N" #'isearch-repeat-backward)
(define-key global-map (kbd "s-;") #'eval-expression)
(define-key global-map (kbd "C-c a") #'org-agenda)
(define-key global-map (kbd "C-c c") #'org-capture)

(add-hook 'emacs-lisp-mode-hook #'evil-paredit-mode)
(add-hook 'emacs-lisp-mode-hook #'nameless-mode)
(add-hook 'emacs-lisp-mode-hook #'paredit-mode)
(add-hook 'clojure-mode-hook #'evil-paredit-mode)
(add-hook 'clojure-mode-hook #'paredit-mode)
(add-hook 'magit-mode-hook (~disable evil-local-mode))
(add-hook 'neotree-mode-hook (~disable evil-local-mode))
(add-hook 'help-mode-hook (~disable evil-local-mode))
(add-hook 'prog-mode-hook #'flycheck-mode)
(add-hook 'haskell-mode-hook #'intero-mode)
(add-hook 'rust-mode-hook #'cargo-minor-mode)
(add-hook 'rust-mode-hook (~disable flycheck-mode)) ;; locks build directory
(add-hook 'dired-mode-hook #'diredfl-mode)

(add-hook 'c-mode-hook #'irony-mode)
(with-hook c-mode-hook
  (c-set-style "linux"))
(with-hook perl-mode-hook
  (setq indent-tabs-mode nil))
(with-hook emacs-lisp-mode-hook
  (setq indent-tabs-mode nil))
(menu-bar-mode -1)
(set-face-attribute 'default nil :family "Terminus" :weight 'bold :height 140)

(setq backup-by-copying t)
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq create-lockfiles nil)
(setq css-indent-offset 8)
(setq custom-file "/dev/null")
(setq enable-local-variables nil)
(setq evil-shift-width 2)
(setq evil-symbol-word-search :symbol-search)
(setq nameless-private-prefix t)
(setq tags-revert-without-query t)
(setq truncate-partial-width-windows t)
(setq org-directory "~/prefix/data/org")
(setq org-capture-templates
      '(("s" "Lifestory" plain (file+olp+datetree "lifestory.org") "%U\n")
        ("l" "Learned"   plain (file+olp+datetree "wisdom.org") "%a\n")))
(dolist (face '(org-level-1 org-level-2 org-level-3))
  (set-face-attribute face nil :height 1.0))

(setq-default display-line-numbers nil)
(setq-default whitespace-style '(tab-mark))
(setq-default fill-column 79)

(defalias 'evil-quit (run (kill-buffer (current-buffer))))
(defalias 'yes-or-no-p #'y-or-n-p)

(push "~/prefix/config/emacs" load-path)
(add-to-list 'company-backends 'company-irony)
(add-to-list 'company-backends 'company-c-headers)

(when (file-readable-p "~/.emacs.override")
  (add-to-list 'auto-mode-alist '(".emacs.override" . emacs-lisp-mode))
  (load "~/.emacs.override"))

(fullframe magit-status quit-window nil)
