[![pipeline status](https://gitlab.com/KAction/config/badges/master/pipeline.svg)](https://gitlab.com/KAction/config/commits/master)

Opinionated solution for configuration files
============================================

Most of us, sooner or later, encounter question -- how to manage
configuration files to all you programs -- editors, shells, mail client
and so on. I tried many things, and many things failed for me. After
many searches and experiments, I found simple solution -- plain archive
`config.tar.gz`.

I just extract that archive onto home directory, and I am at home. No
additional software is required -- `tar` and `gzip` are essential tools,
provided by any GNU/Linux environment, even `busybox`-based.
That is it -- all my configuration, all my scripts are always
with me, in single file. Nothing could be simplier.

On other hand, to *build* that file, I use quite advanced tools -- `tup`
build system in particular.

Warning
-------

Please, keep in mind, that extracting `config.tar.gz` will silently
overwrite already existing files. Backup your own config files!
